{ obelisk ? import ./.obelisk/impl {
    system = builtins.currentSystem;
    iosSdkVersion = "10.2";
    # You must accept the Android Software Development Kit License Agreement at
    # https://developer.android.com/studio/terms in order to build Android apps.
    # Uncomment and set this to `true` to indicate your acceptance:
    # config.android_sdk.accept_license = false;
  }
  , withHoogle ? false
}:
let deps = obelisk.nixpkgs.thunkSet ./dep;
    haskellLib = obelisk.nixpkgs.haskell.lib;
in with obelisk;
project ./. ({ pkgs, hackGet, ... }: {
  packages = {
    # database-id-* was recently put on hackage.
    database-id-class = deps.database-id + /class;
    database-id-groundhog = deps.database-id + /groundhog;
    database-id-obelisk = deps.database-id + /obelisk;

    # gargoyle-postgresql-connect is not yet on hackage.
    gargoyle-postgresql-connect = deps.gargoyle + /gargoyle-postgresql-connect;
  };
  overrides = self: super: {
    # Using a fork of groundhog.
    groundhog = self.callCabal2nix "groundhog" (deps.groundhog + /groundhog) {};
    groundhog-postgresql = self.callCabal2nix "groundhog" (deps.groundhog + /groundhog-postgresql) {};
    groundhog-th = self.callCabal2nix "groundhog" (deps.groundhog + /groundhog-th) {};

    # aeson-gadt-th requires this override to build on ios due to an upstream issue.
    aeson-gadt-th = (self.callCabal2nix "aeson-gadt-th" deps.aeson-gadt-th {}).overrideAttrs (drv: {
      configureFlags = drv.configureFlags or [] ++ ["-f-build-readme"]; # Upstream issue: readme doesn't build on ios.
    });

    # Use a newer version of gargoyle
    gargoyle = self.callCabal2nix "gargoyle" (deps.gargoyle + /gargoyle) {};
    gargoyle-postgresql = self.callCabal2nix "gargoyle" (deps.gargoyle + /gargoyle-postgresql) {};
    # Use callPackage instead of callCabal2nix here to use the default.nix
    # file rather than the cabal file so that we get
    # the non-haskell dependencies (e.g., pg_ctl) as well.
    gargoyle-postgresql-nix = self.callPackage (deps.gargoyle + /gargoyle-postgresql-nix) {};
  };
  android.applicationId = "systems.obsidian.obelisk.examples.minimal";
  android.displayName = "Obelisk Minimal Example";
  ios.bundleIdentifier = "systems.obsidian.obelisk.examples.minimal";
  ios.bundleName = "Obelisk Minimal Example";

  inherit withHoogle;
})
