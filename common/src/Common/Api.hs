{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE UndecidableInstances #-}
module Common.Api where

import Common.Schema
import Data.Aeson
import Data.Aeson.Types (typeMismatch)
import Data.Aeson.GADT.TH
import Data.ByteString (ByteString)
import Data.Constraint.Extras.TH
import Data.Text (Text)
import qualified Data.Text.Encoding as T
import Database.Id.Class
import qualified Data.ByteString.Base64 as B64
import Reflex

newtype Token = Token ByteString
  deriving Show

instance ToJSON Token where
  toJSON (Token bs) = String (T.decodeUtf8 (B64.encode bs))

instance FromJSON Token where
  parseJSON (String x) = case B64.decode (T.encodeUtf8 x) of
    Left _ -> fail "Couldn't base64 decode Token."
    Right t -> return $ Token t
  parseJSON v = typeMismatch "String" v

data Api :: * -> * where
  Api_Login :: Text -> Text -> Api (Maybe (Text, Token))
  Api_CreatePost :: Token -> Text -> Text -> Api (Id Post)
  Api_GetPosts :: Api [(Id Post, Post)] -- TODO Pagination
  Api_GetPost :: Id Post -> Api (Maybe Post)

type Lithograph t m = (Requester t m, Request m ~ Api, Response m ~ Either Text)

deriveArgDict ''Api
deriveJSONGADT ''Api
