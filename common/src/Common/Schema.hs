{-# LANGUAGE DeriveGeneric #-}
module Common.Schema where

import Database.Id.Class
import Data.Aeson
import Data.ByteString (ByteString)
import Data.Text (Text)
import Data.Time (UTCTime)
import GHC.Generics

data Account = Account
  { account_email :: Text
  , account_passwordHash :: Maybe ByteString
  , account_passwordResetNonce :: Maybe UTCTime
  }
  deriving (Eq, Ord, Show)

instance HasId Account

data Post = Post
  { post_title :: Text
  , post_body :: Text
  , post_timestamp :: UTCTime
  }
  deriving (Generic)

instance HasId Post
instance ToJSON Post
instance FromJSON Post
