{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecursiveDo #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications #-}
module Frontend where

import Control.Concurrent (newEmptyMVar, putMVar, takeMVar)
import Control.Monad (forM_, (<=<))
import Control.Monad.Fix (MonadFix)
import Control.Monad.IO.Class (liftIO, MonadIO)
import qualified Data.ByteString.Lazy as LBS
import Data.Constraint.Extras (has)
import qualified Data.Aeson as Aeson
import Data.Text (Text)
import qualified Data.Text as T
import qualified Data.Text.Encoding as T
import Database.Id.Class
import Language.Javascript.JSaddle (MonadJSM)
import Reflex.Dom.Core

import Obelisk.Frontend (Frontend(..))
import Obelisk.Generated.Static
import Obelisk.Route ()
import Obelisk.Route.Frontend

import Common.Api
import Common.Route
import Common.Schema

frontend :: Frontend (R FrontendRoute)
frontend = Frontend
  { _frontend_head = do
    el "title" $ text "Lithograph: Obsidian Blog"
    elAttr "link" ("rel" =: "stylesheet" <> "href" =: static @"flat-remix.min.css") blank
    elAttr "link" ("rel" =: "stylesheet" <> "href" =: static @"main.css") blank
  , _frontend_body = do

    -- A validated route encoder can be used to render backend routes for use, e.g.,
    -- in constructing xhr endpoint urls
    let Right validEncoder = checkEncoder fullRouteEncoder

    -- The url of our single api endpoint. We send json-encoded 'Api' requests (see
    -- Common.Api) to this endpoint and case on the deserialized requests. We could
    -- also specify separate routes for different APIs, but given that this is an
    -- internal rather than a published API, there's not much point.
    let apiRoute = renderBackendRoute validEncoder (BackendRoute_Api :/ ())

    -- The recursive block below pairs up requests with responses. 'RequesterT' gives
    -- our 'app' the ability to make requests that are dispatched to the backend.
    -- `mapRoutedT` is used to ensure that RoutedT remains the outermost transformer.
    rec ((), req) <- mapRoutedT (flip runRequesterT $ switchPromptlyDyn rsp) app
        -- The 'prerender' here does nothing during static rendering and actually
        -- makes requests after "hydration"
        rsp <- prerender (return never) $
          performEventAsync $ ffor req $ \r yield ->
            liftIO . yield =<< mkRequests apiRoute r
    return ()
  }

-- | Transforms an 'Api' request to its XHR representation and attempts
-- to decode the response.
-- TODO: Switch to websockets
mkRequests
  :: (MonadIO m, HasJSContext m, MonadJSM m)
  => Text
  -> RequesterData Api
  -> m (RequesterData (Either Text))
mkRequests apiRoute = traverseRequesterData $ \x ->
  has @Aeson.FromJSON @Api x $ mkRequest x
  where
    -- TODO: This function can probably be generalized and upstreamed to reflex-dom
    mkRequest
      :: (HasJSContext m, MonadJSM m, Aeson.FromJSON b)
      => Api b
      -> m (Either Text b)
    mkRequest req = do
      response <- liftIO newEmptyMVar
      _ <- newXMLHttpRequest (postJson apiRoute req) $ liftIO . putMVar response
      xhrResp <- liftIO $ takeMVar response
      case decodeXhrResponse xhrResp of
        Nothing -> return $ Left $
          "Error: mkRequests: Response could not be decoded for request: " <>
            T.decodeUtf8 (LBS.toStrict $ Aeson.encode req)
        Just r -> return $ Right r

navigationLinks
  :: ( DomBuilder t m
     , SetRoute t (R FrontendRoute) m
     , RouteToUrl (R FrontendRoute) m
     )
  => m ()
navigationLinks = do
  el "nav" $ do
  el "h1" $ routeLink (FrontendRoute_Main :/ ()) $ text "Lithograph"
  el "ul" $ do
    el "li" $ routeLink (FrontendRoute_Post :/ PostRoute_List :/ ()) $ text "Recent Posts"
    el "li" $ routeLink (FrontendRoute_Post :/ PostRoute_Compose :/ ()) $ text "Compose New Post"

loginWidget
  :: ( DomBuilder t m
     , PostBuild t m
     , MonadHold t m
     , Lithograph t m
     , MonadFix m
     )
  => m (Dynamic t (Maybe (Text, Token)))
loginWidget = do
  divClass "login-widget" $ do
    rec
      -- Here, we display a different widget depending on whether the user is currently
      -- logged in or not by examining:
      -- tokenDyn :: Dynamic t (Maybe (Text, Token))
      -- which is defined below, and is also the result of loginWidget.
      result <- switchHold never <=< dyn . ffor tokenDyn $ \case
        -- If the user is not logged in, we display a form for them to enter their
        -- email address and password.
        Nothing -> divClass "login-form" $ do
          username <- el "div" . el "label" $ do
            text "Email"
            fmap value $ inputElement def
          password <- el "div" . el "label" $ do
            text "Password"
            fmap value . inputElement $
              def & inputElementConfig_elementConfig
                  . elementConfig_initialAttributes
                  .~ (AttributeName Nothing "type" =: "password")
          loginSubmit <- el "div" . el "label" $ do
            text "\160"
            loginClick <- button "Log In"
            -- We tag the Event of clicks on the log in button with the current values of
            -- the username and password text fields.
            return $ tag (current (zipDyn username password)) loginClick
          -- Here, we translate the login submit events into API requests to log in.
          loginResponse <- requesting . ffor loginSubmit $ \(user, pw) ->
            Api_Login user pw
          return (Just <$> loginResponse)
        Just (user, _token) -> divClass "login-form" $ do
          el "div" $ text ("Logged in as " <> user)
          logoutLink <- el "div" $ link "Log Out"
          return (Nothing <$ _link_clicked logoutLink)

      loginResult <- holdDyn Nothing result
      divClass "login-error" . dyn_ . ffor loginResult $ \case
        Nothing -> blank -- User hasn't yet attempted to log in.
        Just (Left _) -> text "There was a problem contacting the server"
        Just (Right Nothing) -> text "There was a problem with the provided credentials"
        _ -> blank -- User has correctly logged in.
      let tokenDyn = ffor loginResult $ \case
            Just (Right (Just x)) -> Just x
            _ -> Nothing
    return tokenDyn

-- | The main application body entry point
app
  :: ( DomBuilder t m
     , MonadHold t m
     , Prerender js t m
     , MonadFix m
     , RouteToUrl (R FrontendRoute) m
     , SetRoute t (R FrontendRoute) m
     , Response m ~ Either Text
     , Request m ~ Api
     , Requester t m
     , PostBuild t m
     )
  => RoutedT t (R FrontendRoute) m ()
app = do
  token <- elAttr "div" ("class" =: "top") $ do
    navigationLinks
    loginWidget
      -- produces a Dynamic t (Maybe (Text, Token)) with the user's email and
      -- token if they're signed in.
  subRoute_ $ \case
    FrontendRoute_Main -> do
      el "h1" $ text "A Blog Built with Obelisk"
      el "p" $ text "Welcome to Lithograph. This is an example app demonstrating some of the features of Obelisk. Click the links above to explore the application, which implements a simplified blogging framework."
      el "p" $ do
        text "For the code associated with this project, check out the "
        elAttr "a" ("href" =: "https://gitlab.com/obsidian.systems/lithograph" <> "target" =: "_blank") $ text "repository"
        text "."
    FrontendRoute_Post -> do
      rec newPost :: Dynamic t (Event t (Either Text (Id Post))) <- subRoute $ \case
            PostRoute_List -> postList (() <$ (switch $ current newPost)) >> return never
            PostRoute_Compose -> compose token
            PostRoute_Post -> do
              pid <- askRoute
              pb <- getPostBuild'
              mpost <- requesting $ Api_GetPost <$> leftmost [updated pid, tag (current pid) pb]
              widgetHold_ (text "Loading...") $ ffor mpost $ \case
                Right (Just p) -> singlePost p $ mapM_ (el "p" . text) . T.lines
                _ -> text "Couldn't load post."
              return never
      return ()

-- | Form to compose a new blog post
compose
  :: ( DomBuilder t m
     , Lithograph t m
     , MonadHold t m
     , PostBuild t m
     )
  => Dynamic t (Maybe (Text, Token))
  -> m (Event t (Either Text (Id Post)))
compose token = divClass "post-form" $ do
  switchHold never <=< dyn . ffor token $ \case
    Nothing -> do
      text "Please log in to be able to post to this blog."
      return never
    Just (_, tok) -> do
      title <- inputElement $ def
        & inputElementConfig_elementConfig
        . elementConfig_initialAttributes
        .~ ("placeholder" =: "Post title")
      body <- textAreaElement $ def
        & textAreaElementConfig_elementConfig
        . elementConfig_initialAttributes
        .~ ("placeholder" =: "Write your blog post here")
      save <- fmap (domEvent Click . fst) $
        elAttr' "button" ("type" =: "button" <> "class" =: "blue-button") $ text "Save"
      response <- requesting $
        tag (current $ Api_CreatePost tok <$> value title <*> value body) save
      widgetHold_ blank $ text . T.pack . show <$> response
      return response

-- | List blog posts, truncating body
postList
  :: ( DomBuilder t m
     , Response m ~ Either Text
     , Request m ~ Api
     , Requester t m
     , MonadHold t m
     , Prerender js t m
     , SetRoute t (R FrontendRoute) m
     , RouteToUrl (R FrontendRoute) m
     )
  => Event t ()
  -> m ()
postList refresh = divClass "post-list" $ do
  pb <- getPostBuild'
  response <- requesting $ Api_GetPosts <$ leftmost [pb, refresh]
  widgetHold_ (text "Loading...") $ ffor response $ \case
    Left err -> text $ "Error loading posts: " <> err
    Right posts -> forM_ posts $ \(pid, p) -> routeLink (FrontendRoute_Post :/ PostRoute_Post :/ pid) $
      singlePost p $ el "p" . text . (\x -> x <> "...") . T.take 100

-- TODO: Should be able to use regular getPostBuild here or provide some alternative
getPostBuild' :: (Monad m, Prerender js t m) => m (Event t ())
getPostBuild' = updated <$> prerender blank blank

-- | Render a single blog post
singlePost :: DomBuilder t m => Post -> (Text -> m a) -> m a
singlePost p renderBody = divClass "paper" $ do
  el "h1" $ text $ post_title p
  el "h4" $ text $ T.pack $ show $ post_timestamp p
  renderBody $ post_body p
