{-# LANGUAGE EmptyCase #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}
module Backend where

import Control.Monad.Logger
import Control.Monad.Trans.Control
import Control.Monad.IO.Class
import Crypto.PasswordStore (verifyPasswordWith, pbkdf2)
import qualified Data.Aeson as Aeson
import Data.Bifunctor
import Data.Constraint.Extras
import Data.Pool
import Data.Some
import Data.Text (Text)
import qualified Data.Text as T
import Data.Text.Encoding (encodeUtf8)
import Data.Time (getCurrentTime)
import Database.Groundhog.Generic.Migration
import Database.Groundhog.Postgresql
import Database.Id.Class
import Database.Id.Groundhog
import Gargoyle.PostgreSQL.Connect (withDb)
import Obelisk.Backend
import Obelisk.Route
import Snap
import System.Directory (createDirectoryIfMissing)
import Web.ClientSession as CS

import Backend.Schema
import Common.Api
import Common.Route
import Common.Schema

-- TODO Find a better home for this function
-- | Run a database transaction using a given postgres resource pool
runDb
  :: (MonadBaseControl IO m, MonadIO m)
  => Pool Postgresql
  -> (DbPersist Postgresql (NoLoggingT m)  a)
  -> m a
runDb pool = runNoLoggingT . withResource pool . runDbConn

encryptToken :: CS.Key -> Account -> IO Token
encryptToken csk acc = do
  tok <- CS.encryptIO csk (encodeUtf8 $ account_email acc)
  return (Token tok)

decryptToken :: CS.Key -> Token -> Maybe Text
decryptToken csk (Token tok) = do
  dec <- CS.decrypt csk tok
  email <- Aeson.decodeStrict' dec
  return email

checkToken :: (PersistBackend m) => CS.Key -> Token -> m (Maybe Account)
checkToken csk token = do
  case decryptToken csk token of
    Nothing -> return Nothing
    Just email -> do
      accs <- project AccountConstructor (Account_emailField ==. email)
      case accs of
        [] -> return Nothing
        [acc] -> return (Just acc)
        _ -> fail "checkToken: Account uniqueness constraint violated"

backend :: Backend BackendRoute FrontendRoute
backend = Backend
  { _backend_run = \serve -> do
    -- Retrieve or initialise the client session key, used to encrypt login tokens.
    createDirectoryIfMissing True "config/backend"
    csk <- CS.getKey "config/backend/clientSessionKey"
    -- Initialize the database and connect
    liftIO $ withDb "db" $ \pool -> do
      runDb pool $ do
        -- Migrate teh database. See Backend.Schema for template haskell that
        -- generates the 'migrateDb' function
        ta <- getTableAnalysis
        runMigration $ migrateDb ta
      -- Handle 'BackendRoute's
      serve $ \case
        -- The Backend_Api route is expected to receive json-encoded API requests
        -- conforming to the 'Api' specification in Common.Api
        BackendRoute_Api :/ () -> do
          b <- readRequestBody (1000000)
          case Aeson.eitherDecode b of
            Left err -> do
              modifyResponse $ setResponseStatus 400 "Malformed request"
              writeText $ "Malformed request: " <> T.pack err <> "\n"

            Right (Some reqBody) -> do
              resp <- requestHandler pool csk reqBody
              writeLBS $ (has @Aeson.ToJSON @Api reqBody $ Aeson.encode resp)

        _ -> return ()
  , _backend_routeEncoder = fullRouteEncoder
  }

requestHandler :: (Monad m, MonadBaseControl IO m, MonadSnap m) => Pool Postgresql -> CS.Key -> Api resp -> m resp
requestHandler pool csk req = runDb pool $ case req of
  Api_Login email password -> do
    accs <-
      project AccountConstructor (Account_emailField ==. email)
    result <- case accs of
      [] -> do
        return Nothing
      (acc:_) -> do
        let passwordValid = case account_passwordHash acc of
              Nothing -> False
              Just ph -> verifyPasswordWith pbkdf2 (2^)
                (encodeUtf8 password) ph
        if passwordValid
          then do
            token <- liftIO $ encryptToken csk acc
            return (Just (email, token))
          else return Nothing
    return result
  Api_CreatePost token title body -> do
    _ <- checkToken csk token -- Check that the user's account exists
    t <- liftIO $ getCurrentTime -- TODO get time from db
    k :: Id Post <- fmap toId $
      insert $ Post title body t
    return k
  Api_GetPosts -> do
    posts :: [(Id Post, Post)] <- fmap (fmap (first toId)) $
      project (AutoKeyField, PostConstructor) $
        CondEmpty `orderBy` [Desc Post_timestampField] `limitTo` 10
    return posts
  Api_GetPost pid -> do
    post <- get $ fromId pid
    return post
