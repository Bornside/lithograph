module Backend.Commands where

import Control.Monad.Logger
import Control.Monad.Trans.Control
import Control.Monad.IO.Class
import Crypto.PasswordStore (pbkdf2, genSaltIO, makePasswordSaltWith)
import qualified Data.Aeson as Aeson
import Data.Bifunctor
import Data.Pool
import Data.Some
import Data.Text.Encoding (encodeUtf8)
import Data.Time (getCurrentTime)
import Database.Groundhog.Generic.Migration
import Database.Groundhog.Postgresql
import Database.Id.Class
import Database.Id.Groundhog
import Gargoyle.PostgreSQL.Connect (withDb)
import Obelisk.Backend
import Obelisk.Route
import Snap
import System.Directory (createDirectoryIfMissing)
import Web.ClientSession as CS

import Backend.Schema
import Common.Api
import Common.Route
import Common.Schema

import qualified Data.Text as T
import Data.Text (Text)
import Data.ByteString (ByteString)
import System.Environment (getArgs)
import Backend

makePasswordHash
  :: MonadIO m
  => Text
  -> m ByteString
makePasswordHash pw = do
  salt <- liftIO genSaltIO
  return $ makePasswordSaltWith pbkdf2 (2^) (encodeUtf8 pw) salt 14

createAccount = do
  xs <- getArgs
  case xs of
    [email,pw] -> do
      hash <- makePasswordHash (T.pack pw)
      withDb "db" $ \pool -> runDb pool $
        insert $ Account
          { account_email = T.pack email
          , account_passwordHash = Just hash
          , account_passwordResetNonce = Nothing
          }
      putStrLn "Added user."
    _ -> putStrLn "Usage: create-password EMAIL PASSWORD"
