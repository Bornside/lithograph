{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

{-# OPTIONS_GHC -fno-warn-orphans #-} -- mkPersist will produce orphan instances
{-# OPTIONS_GHC -fno-warn-unused-matches #-} -- mkPersist sometimes produces unused matches
module Backend.Schema where

import Common.Schema
import Database.Groundhog ()
import Database.Groundhog.TH
import Database.Id.Groundhog.TH

mkPersist (defaultCodegenConfig { migrationFunction = Just "migrateDb" }) [groundhog|
  - entity: Account
    constructors:
      - name: Account
        uniques:
          - name: emailUnique
            type: index
            fields: [{expr: "lower(account_email::text)"}]
  - entity: Post
|]

makeDefaultKeyIdInt64 ''Account 'AccountKey
makeDefaultKeyIdInt64 ''Post 'PostKey
